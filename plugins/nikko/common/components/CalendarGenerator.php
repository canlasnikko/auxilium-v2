<?php namespace Nikko\Common\Components;

use Cms\Classes\ComponentBase;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class CalendarGenerator extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CalendarGenerator Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function generate_dates($date)
    {
        $start = Carbon::parse($date)->startOfMonth();
        $end = Carbon::parse($date)->endOfMonth();

        $dates_object = new \stdClass();
        $dates_object->englishMonth = $start->englishMonth;
        $dates_object->month = $start->month;
        $dates_object->year = $start->year;


        $previousMonth = Carbon::parse($date)->subMonth()->format('Y-m');
        $nextMonth = Carbon::parse($date)->addMonth()->format('Y-m');

        $dates_object->previousMonth = $previousMonth;
        $dates_object->nextMonth = $nextMonth;

        $dates = [];
        // Appending blank dates 
        if($start->dayOfWeekIso > 1) {
            $i = 1;
            while ( $i < $start->dayOfWeekIso) {
                $date_details = new \stdClass();
                $date_details->day = $i;
                $date_details->date = "";
                array_push($dates, $date_details);
                $i++;
            }

        }

        while ($start->lte($end)) {
            $date_details = new \stdClass();
            $date_details->day = $start->dayOfWeekIso;
            $date_details->date = $start->day;
            $date_details->full_date = $start->format('Ymd');

            $dates[] = $date_details;
            $start->addDay();
        }

        $dates_object->dates = $dates;

        return $dates_object;
    }
}
