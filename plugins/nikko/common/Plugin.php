<?php namespace Nikko\Common;

use Backend;
use System\Classes\PluginBase;

/**
 * Common Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Common',
            'description' => 'No description provided yet...',
            'author'      => 'Nikko',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Nikko\Common\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'nikko.common.some_permission' => [
                'tab' => 'Common',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'common' => [
                'label'       => 'Common',
                'url'         => Backend::url('nikko/common/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['nikko.common.*'],
                'order'       => 500,
            ],
        ];
    }
}
