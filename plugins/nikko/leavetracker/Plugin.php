<?php namespace Nikko\LeaveTracker;

use Backend;
use System\Classes\PluginBase;

/**
 * LeaveTracker Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Leave Tracker',
            'description' => 'Plugin for the Leave Tracker app',
            'author'      => 'Nikko',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Nikko\LeaveTracker\Components\CalendarGenerator' => 'calendarGenerator',
            'Nikko\LeaveTracker\Components\LeaveOverview' => 'leaveOverview',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'nikko.leavetracker.some_permission' => [
                'tab' => 'LeaveTracker',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'leavetracker' => [
                'label'       => 'LeaveTracker',
                'url'         => Backend::url('nikko/leavetracker/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['nikko.leavetracker.*'],
                'order'       => 500,
            ],
        ];
    }
}
