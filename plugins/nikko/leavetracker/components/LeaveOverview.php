<?php namespace Nikko\LeaveTracker\Components;

use Cms\Classes\ComponentBase;
use Carbon\Carbon;
use Nikko\Common\Components\CalendarGenerator;

class LeaveOverview extends ComponentBase
{
    /**
     * The collection of calendar
     * @var string
     */
    public $calendar;

    public function componentDetails()
    {
        return [
            'name'        => 'LeaveOverview Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $date = get('date');
        if(empty($date)) {
            $date = Carbon::now()->format('Y-M');
        }
        $calendarGenerator = new CalendarGenerator();

        $calendar = $calendarGenerator->generate_dates($date);
        
        $this->calendar = $calendar;
    }
}
