<?php namespace Nikko\StatusReport\Components;

use Cms\Classes\ComponentBase;
use Carbon\Carbon;
use Nikko\Common\Components\CalendarGenerator;
use Nikko\StatusReport\Models\Reports;
use Nikko\AccountManagement\Models\Users;

class GenerateReport extends ComponentBase
{
    /**
     * The collection of status reports
     * @var array
     */
    public $status_reports;

    public function componentDetails()
    {
        return [
            'name'        => 'GenerateReport Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = \Auth::getUser();

        if(empty($user)) return \Redirect::to('/');

        $date = get('date');
        if(empty($date)) {
            $date = Carbon::now()->format('Y-M');
        }
        $calendarGenerator = new CalendarGenerator();

        $status_reports = $calendarGenerator->generate_dates($date);

        $i = 0;
        $days_worked = 0;
        foreach ($status_reports->dates as $date) {
            if(empty($date->date)) {
                unset($status_reports->dates[$i]);
            } else {

                $report = Reports::where('user_id', $user->id)
                ->where('date', $date->full_date)
                ->first();

                $date->description = (isset($report->description))? $report->description : '';
                $date->title = (isset($report->title))? $report->title : '';
                $date->image = (isset($report->image))? $report->image : '';
                $date->weekday  = Carbon::parse($date->full_date)->englishDayOfWeek;
                $date->full_date = Carbon::parse($date->full_date)->format('d-m-Y');

                if(!empty($date->title) && !in_array($date->title, array( 'AL', 'PH', 'MC')) && $date->day != '6' && $date->day != '7') {
                    $days_worked++;
                }
            }
            $i++;
        }

        $acct_user = Users::where('auth_user_id', $user->id)->first();

        $report_style = $this->generate_report_style($status_reports, $user, $acct_user, $days_worked);
        $this->status_reports = $report_style;

        // $report_status = $this->generate_report_word($status_reports);
        // $this->status_reports = $status_reports;
    }

    private function generate_report_style($status_reports, $auth_user, $acct_user, $days_worked = 0)
    {

        // var_dump($status_reports);die;
        $html = 
        '<html><body>'.
        '<span><u><strong>E-Commerce Resource Augmentation: Work Done for webe digital in '. $status_reports->englishMonth . ' ' . $status_reports->year .'</strong></u></span><br />'.
        '<span>Name ('. $acct_user->eid .'): '. $acct_user->name .'</span><br />'.
        '<span>Role: '. $acct_user->role .'</span><br />'.
        '<span>Days Worked: '. $days_worked .'</span><br /><br />'.
        '<table>'.
            '<tr>'.
                '<th colspan="2">Day of Month</th>'.
                '<th rowspan="2" style="width: 300px;">Work Done</th>'.
                '<th rowspan="2" style="width: 300px;">Proof of Task</th>'.
            '</tr>'.
            '<tr>'.
                '<th style="width: 100px;">Date</th>'.
                '<th style="width: 100px;">Day</th>'.
            '</tr>';

        $data = '';

        foreach ($status_reports->dates as $status_report) {
            if(in_array($status_report->title, array( 'AL', 'PH', 'MC')) || $status_report->day == '6' || $status_report->day == '7') {
                $data = $data . '<tr style="background-color:lightgray">';
                
            } else {
                $data = $data . '<tr>';
            }
            $data = $data . 
                    '<td>' . $status_report->full_date . '</td>' . 
                    '<td>' . $status_report->weekday . '</td>' . 
                    '<td class="description">' . $status_report->description . '</td>' . 
                    '<td class="proof">' . $status_report->image . '</td>' .
                '</tr>';
        }

        $data = $data;

        $html = $html . $data . '</table></body></html>';

        $style = '<style type="text/css">
                  table {
                    table-layout: fixed; 
                    width: 800px;
                    border-collapse: collapse;
                  }
                  th {
                    background: #273660;
                    color: white;
                    font-weight: bold;
                    vertical-align: middle;
                  }
                  td, tr, th {
                      /*width:30%;*/
                      position:relative;
                      overflow: hidden;
                      border: 1px;
                      border-style: solid;
                  }
                  .description {
                    text-align: left;
                  }
                </style></body></html>';

        $html = $html . $style;

        return $html;
    }

    // private function generate_report_word($status_reports)
    // {
    //     // echo '<pre>'.$report_style.'</pre>';die;
    //     // var_dump($report_style);die;
    //     $report_style = '<html><body>'.
    //     '<table style="table-layout: fixed; width: 800px;">
    //         <tr style="position:relative; overflow: hidden; border: 1px; border: 1px;">
    //           <th colspan="2" style="background: #273660; color: white; font-weight: bold; vertical-align: middle; position:relative; overflow: hidden; border: 1px; border-style: solid;">Day of Month</th>
    //           <th rowspan="2" style="width: 300px; background: #273660; color: white; font-weight: bold; vertical-align: middle; position:relative; overflow: hidden; border: 1px; border-style: solid;">Work Done</th>
    //           <th rowspan="2" style="width: 300px; background: #273660; color: white; font-weight: bold; vertical-align: middle; position:relative; overflow: hidden; border: 1px; border-style: solid;">Proof of Task</th>
    //         </tr>
    //         <tr style="position:relative; overflow: hidden; border: 1px; border: 1px;">
    //           <th style="width: 100px; background: #273660; color: white; font-weight: bold; vertical-align: middle; position:relative; overflow: hidden; border: 1px; border-style: solid;">Date</th>
    //           <th style="width: 100px; background: #273660; color: white; font-weight: bold; vertical-align: middle; position:relative; overflow: hidden; border: 1px; border-style: solid;">Day</th>
    //         </tr></table></body></html>';

    //     $phpWord = new \PhpOffice\PhpWord\PhpWord();
    //     $phpWord->setDefaultFontName('Calibri');
    //     $phpWord->setDefaultFontSize(11);

    //     $section = $phpWord->addSection();

    //     $sectionStyle = $section->getStyle();
    //     $sectionStyle->setMarginLeft(\PhpOffice\PhpWord\Shared\Converter::inchToTwip(.5));
    //     $sectionStyle->setMarginRight(\PhpOffice\PhpWord\Shared\Converter::cmToTwip(.5));

    //     $header = 'E-Commerce Resource Augmentation: Work Done for webe digital in October 2018';
    //     $name = 'Name (nikko.l.canlas): Nikko L. Canlas';
    //     $role = 'Role: E-Comm: Web Lumen';
    //     $days_worked = 'Days Worked: 1';


    //     $section->addText($header, array('bold' => true));
    //     $section->addText($name);
    //     $section->addText($role);
    //     $section->addText($days_worked);
    //     // $section->addTextBreak(1);

    //     \PhpOffice\PhpWord\Shared\Html::addHtml($section, $report_style);
    //     header('Content-Type: application/octet-stream');
    //     header('Content-Disposition: attachment;filename="test.docx"');

    //     // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    //     $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    //     try {
    //         // $objWriter->save(storage_path('TestWordFile.docx'));
    //         $objWriter->save('php://output');

    //     } catch (Exception $e) {
    //         return false;
    //     }

    //     return true;
    // }
}


