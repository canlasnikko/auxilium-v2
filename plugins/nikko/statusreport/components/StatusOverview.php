<?php namespace Nikko\StatusReport\Components;

use Cms\Classes\ComponentBase;
use Carbon\Carbon;
use Nikko\Common\Components\CalendarGenerator;
use Nikko\AccountManagement\Components\PageForm;
use Nikko\StatusReport\Models\Reports;

class StatusOverview extends ComponentBase
{
    /**
     * The collection of calendar
     * @var array
     */
    public $calendar;
    
    public function componentDetails()
    {
        return [
            'name'        => 'StatusOverview Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = \Auth::getUser();

        if(empty($user)) return \Redirect::to('/');

        $date = get('date');
        if(empty($date)) {
            $date = Carbon::now()->format('Y-M');
        }
        $calendarGenerator = new CalendarGenerator();

        $calendar = $calendarGenerator->generate_dates($date);

        foreach ($calendar->dates as $date) {
            if(empty($date->date)) continue;

            $report = Reports::where('user_id', $user->id)
            ->where('date', $date->full_date)
            ->first();

            $date->status_description = (isset($report->description))? $report->description : '';
            $date->status_title = (isset($report->title))? $report->title : '';
            $date->bg_color = 'white';

            if(in_array($date->status_title, array( 'AL', 'PH', 'MC')) || $date->day == '6' || $date->day == '7') {
                $date->bg_color = 'lightgray';
            } elseif (!empty($date->status_title)) {
                $date->bg_color = 'yellowgreen';
            }
        }
        
        $this->calendar = $calendar;
    }
}
