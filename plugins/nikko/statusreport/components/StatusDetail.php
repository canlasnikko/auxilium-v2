<?php namespace Nikko\StatusReport\Components;

use Cms\Classes\ComponentBase;
use Carbon\Carbon;
use Nikko\StatusReport\Models\Reports;

class StatusDetail extends ComponentBase
{
    /**
     * The collection of status
     * @var array
     */
    public $status;

    public function componentDetails()
    {
        return [
            'name'        => 'StatusDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = \Auth::getUser();

        if(empty($user)) return \Redirect::to('/');
        
        $date = get('date');
        if(empty($date)) {
            $date = Carbon::now()->format('Y-M');
        }

        $status = new \stdClass();

        $carbon_date = Carbon::parse($date);

        $status->weekday = $carbon_date->englishDayOfWeek;
        $status->date = $carbon_date->format('Y-m-d');
        $status->report_date = $carbon_date->format('Y-m');
        $status->previous_date = Carbon::parse($date)->subDay()->format('Y-m-d');
        $status->next_date = Carbon::parse($date)->addDay()->format('Y-m-d');

        $date = $carbon_date->format('Ymd');

        $report = Reports::where('user_id', $user->id)
        ->where('date', $date)->first();

        if(!is_null($report)) {
            $status->image = $report->image;
            $status->description = $report->description;
            $status->title = $report->title;
        }

        $this->status = $status;
    }

    public function onStatusSaveAttempt()
    {
        try {
            $user = \Auth::getUser();
            if(empty($user)) return \Redirect::to('/');

            $date = get('date');
            if(empty($date)) {
                $date = Carbon::now()->format('Y-M');
            }

            $carbon_date = Carbon::parse($date);
            $date = $carbon_date->format('Ymd');

            $report = Reports::where('user_id', $user->id)
            ->where('date', $date)->first();

            if(empty($report)) {
                Reports::create([
                    'user_id'       =>  $user->id,
                    'date'          =>  $date,
                    'weekday'       =>  $carbon_date->englishDayOfWeek,
                    'image'         =>  '',
                    'title'         =>  post('title'),
                    'description'   =>  post('description')
                ]);
            } else {
                $report->image = '';
                $report->title = post('title');
                $report->description = post('description');
                $report->save();
            }

            \Flash::success('Status saved');
        } catch (Exception $e) {
            throw new \ValidationException(['error' => $e->getMessage()]);
        }
    }
}
