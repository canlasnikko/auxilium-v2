<?php namespace Nikko\StatusReport\Models;

use Model;

/**
 * Reports Model
 */
class Reports extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'nikko_statusreport_reports';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['user_id', 'date', 'weekday', 'image', 'title', 'description'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
