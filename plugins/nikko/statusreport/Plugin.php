<?php namespace Nikko\StatusReport;

use Backend;
use System\Classes\PluginBase;

/**
 * StatusReport Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'StatusReport',
            'description' => 'No description provided yet...',
            'author'      => 'Nikko',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Nikko\StatusReport\Components\StatusOverview' => 'statusOverview',
            'Nikko\StatusReport\Components\StatusDetail' => 'statusDetail',
            'Nikko\StatusReport\Components\GenerateReport' => 'generateReport',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'nikko.statusreport.some_permission' => [
                'tab' => 'StatusReport',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'statusreport' => [
                'label'       => 'StatusReport',
                'url'         => Backend::url('nikko/statusreport/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['nikko.statusreport.*'],
                'order'       => 500,
            ],
        ];
    }
}
