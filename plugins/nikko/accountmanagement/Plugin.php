<?php namespace Nikko\AccountManagement;

use Backend;
use System\Classes\PluginBase;

/**
 * AccountManagement Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Account Management',
            'description' => 'Plugin for handling anything related to account management',
            'author'      => 'Nikko Lazo Canlas',
            'icon'        => 'icon-user'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Nikko\AccountManagement\Components\PageForm' => 'pageForm',
            'Nikko\AccountManagement\Components\Profile' => 'profile',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'nikko.accountmanagement.some_permission' => [
                'tab' => 'AccountManagement',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'accountmanagement' => [
                'label'       => 'AccountManagement',
                'url'         => Backend::url('nikko/accountmanagement/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['nikko.accountmanagement.*'],
                'order'       => 500,
            ],
        ];
    }
}
