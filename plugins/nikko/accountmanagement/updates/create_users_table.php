<?php namespace Nikko\AccountManagement\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('nikko_accountmanagement_users', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('auth_user_id');
            $table->string('name');
            $table->string('email');
            $table->string('eid');
            $table->string('role');
            $table->string('type');
            $table->string('referrer_code')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nikko_accountmanagement_users');
    }
}
