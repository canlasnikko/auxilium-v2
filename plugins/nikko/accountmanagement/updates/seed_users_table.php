<?php namespace Nikko\AccountManagement\Updates;

use Seeder;
use Nikko\AccountManagement\Models\Users;

class SeedUsersTable extends Seeder
{
    public function run()
    {
        $user = Users::create([
            'email'                 => 'user@example.com',
            'login'                 => 'user',
            'password'              => 'password123',
            'password_confirmation' => 'password123',
            'first_name'            => 'Actual',
            'last_name'             => 'Person',
            'is_activated'          => true
        ]);
    }
}