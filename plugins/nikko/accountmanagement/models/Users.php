<?php namespace Nikko\AccountManagement\Models;

use Model;

/**
 * Users Model
 */
class Users extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'nikko_accountmanagement_users';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['auth_user_id', 'name', 'email', 'eid', 'role', 'type', 'referrer_code'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
