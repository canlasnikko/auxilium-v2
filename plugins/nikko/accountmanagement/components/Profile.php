<?php namespace Nikko\AccountManagement\Components;

use Cms\Classes\ComponentBase;
use Nikko\AccountManagement\Models\Users;

class Profile extends ComponentBase
{
    /**
     * The collection of profile details
     * @var array
     */
    public $profile_details;

    public function componentDetails()
    {
        return [
            'name'        => 'Profile Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = \Auth::getUser();

        if(empty($user)) return \Redirect::to('/');

        $profile = Users::where('auth_user_id', $user->id)->first();

        $profile_obj = new \stdClass();

        $profile_obj->email = $profile->email;
        $profile_obj->name = $profile->name;
        $profile_obj->eid = $profile->eid;
        $profile_obj->role = $profile->role;

        $this->profile_details = $profile_obj;
    }

    public function onChangeDetailsAttempt()
    {
        $user = \Auth::getUser();

        if(empty($user)) return \Redirect::to('/');

        $rules = [
            'name'     =>  ['required'],
            'eid'     =>  ['required'],
            'role'     =>  ['required'],
        ];

        $validator = app('validator')->make(post(), $rules);

        if($validator->fails()) {
            throw new \ValidationException($validator);
        }

        try {

            $profile = Users::where('auth_user_id', $user->id)->first();

            $profile->name = post('name');
            $profile->eid = post('eid');
            $profile->role = post('role');
            $profile->save();

        } catch(\Exception $e) {
            throw new \ValidationException(['error' => $e->getMessage()]);
        }

        \Flash::success('Profile Updated Successfully');
    }
}
