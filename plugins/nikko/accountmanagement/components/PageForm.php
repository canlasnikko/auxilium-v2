<?php namespace Nikko\AccountManagement\Components;

use Cms\Classes\ComponentBase;
use October\Rain\Auth\Manager;
use Nikko\AccountManagement\Models\Users;

class PageForm extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'PageForm Component',
            'description' => 'Component that will handle form ajax requests'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        //Remove all user session before login attempt
        \Auth::logout();
    }

    public function onLoginAttempt()
    {
        $rules = [
            'email'     =>  ['required'],
            'password'  =>  ['required'],
            // 'usertype'  =>  ['required']
        ];

        $validator = app('validator')->make(post(), $rules);

        if($validator->fails()) {
            throw new \ValidationException(['error' => 'Complete all required fields']);
        }

        try {
            $user = \Auth::authenticate([
                'login' => post('email'),
                'password' => post('password')
            ]);

        } catch(\October\Rain\Auth\AuthException $e) {
            throw new \ValidationException(['error' => 'Invalid Creadentials']);
        }

        // \Flash::success(json_encode($user));

        return \Redirect::to('/landing');
    }

    public function onRegisterationAttempt()
    {
        $rules = [
            'name'     =>  ['required'],
            'email'     =>  ['required', 'email'],
            'eid'     =>  ['required'],
            'role'     =>  ['required'],
            'password'  =>  ['required'],
            'confirm_password'    => ['required', 'same:password'],
            // 'usertype'  =>  ['required']
        ];

        $validator = app('validator')->make(post(), $rules);

        if($validator->fails()) {
            throw new \ValidationException($validator);
        }

        if (strpos(post('email'), 'accenture') === false) {
            throw new \ValidationException(['error' => 'Not Allowed']);
        }

        try {
            $user = \Auth::register([
                'name' => post('name'),
                'email' => post('email'),
                'password' => post('password'),
                'password_confirmation' => post('confirm_password'),
            ], true);

            $account_user = Users::create([
                'name' => post('name'),
                'email' => post('email'),
                'eid' => post('eid'),
                'role' => post('role'),
                'auth_user_id' => $user->id
            ]);

        } catch(\October\Rain\Auth\AuthException $e) {
            throw new \ValidationException(['error' => $e->getMessage()]);
        }

        \Flash::success('Registration Successful');
    }
}
